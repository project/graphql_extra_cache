<?php

namespace Drupal\graphql_extra_cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\graphql\GraphQL\Execution\ExecutionResult;

/**
 * A wrapper class for cached GraphQL execution results.
 */
class CachedResult {

  /**
   * Constructor.
   *
   * @param \Drupal\graphql\GraphQL\Execution\ExecutionResult $result
   *   The execution result.
   * @param \Drupal\Core\Cache\CacheableMetadata $metadata
   *   The metadata.
   * @param int $expires
   *   The timestamp at which the result expires.
   */
  public function __construct(
    protected ExecutionResult $result,
    protected CacheableMetadata $metadata,
    protected int $expires,
  ) {
  }

  /**
   * Get the execution result.
   *
   * @return \Drupal\graphql\GraphQL\Execution\ExecutionResult
   *   The execution result.
   */
  public function getExecutionResult(): ExecutionResult {
    return $this->result;
  }

  /**
   * Get the cacheable metadata.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   The cacheable metadata.
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return $this->metadata;
  }

  /**
   * Get the expires time.
   *
   * @return int
   *   The expires time.
   */
  public function getExpires(): int {
    return $this->expires;
  }

}
