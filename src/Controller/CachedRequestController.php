<?php

declare(strict_types=1);

namespace Drupal\graphql_extra_cache\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\graphql\Controller\RequestController;
use Drupal\graphql\Entity\ServerInterface;
use Drupal\graphql\GraphQL\Execution\ExecutionResult;
use Drupal\graphql_extra_cache\CachedResult;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The main GraphQL request handler that will forward to the responsible server.
 */
class CachedRequestController extends RequestController {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->getParameter('graphql.config'),
      $container->get('cache.graphql_extra_cache.response'),
      $container->get('cache_contexts_manager'),
      $container->get('request_stack'),
    );
  }

  /**
   * RequestController constructor.
   *
   * @param array $parameters
   *   The service configuration parameters.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Cache\CacheContextsManager $contextsManager
   *   The cache contexts manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(
    array $parameters,
    protected CacheBackendInterface $cache,
    protected CacheContextsManager $contextsManager,
    protected RequestStack $requestStack
  ) {
    parent::__construct($parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function handleRequest(ServerInterface $graphql_server, $operations): CacheableJsonResponse {
    // We only care about single operations for now.
    if (!is_array($operations)) {
      /** @var \GraphQL\Server\OperationParams $operations */

      // The hash of the query.
      $queryHash = hash('sha256', $operations->query);

      // The hash of the query + variables.
      $operationHash = $this->cachePrefix(
        // @todo We could use the query name here (which is much shorter and
        // might give a slight performance improvement. But this behavior must
        // be configurable.
        $queryHash,
        $operations->operation,
        $operations->variables
      );

      if ($operationHash) {
        // Try to get a cached result.
        $cachedResult = $this->cacheRead($operationHash);
        if ($cachedResult) {
          $response = new CacheableJsonResponse($cachedResult->getExecutionResult());
          $response->addCacheableDependency($cachedResult->getCacheableMetadata());
          return $response;
        }
      }

      // Make sure our persisted query plugin is enabled.
      $persistedQueries = $graphql_server->getPersistedQueryInstances();
      if ($persistedQueries && !empty($persistedQueries['dynamic_cached_persisted_query'])) {
        // Workaround needed here because we can't provide both queryId and query.
        // We pass the original query as a string here.
        // The persisted query plugin then either loads the parsed query from cache
        // or parses it and stores it in the cache.
        $operations->extensions['graphql_extra_cache'] = [
          'query' => $operations->query,
        ];
        // Use the sha256 hash of the query as the ID.
        $operations->queryId = $queryHash;
        // Unset the query.
        $operations->query = NULL;
      }

      // Execute the operation and build the response.
      $result = $graphql_server->executeOperation($operations);
      $response = new CacheableJsonResponse($result);
      $response->addCacheableDependency($result);

      // If we have an operation hash, try to write it in the cache.
      if ($operationHash) {
        $this->cacheWrite($operationHash, $result);
      }

      return $response;
    }

    /** @var \GraphQL\Server\OperationParams[] $operations */
    return $this->handleBatch($graphql_server, $operations);
  }

  /**
   * Calculates the cache prefix from context for the current query.
   *
   * @param string $query
   *   The query body.
   * @param string|null $operation
   *   The name of the operation.
   * @param array|null $variables
   *   The operation variables.
   *
   * @return string|null
   *   The cache prefix or NULL if the given values can't be serialized.
   */
  protected function cachePrefix(string $query, string|null $operation, array|null $variables): string|null {
    if ($variables) {
      // Sort the variables.
      ksort($variables);
    }

    $operationString = $operation ?? 'noop';

    try {
      // The variables as a serialized string.
      $stringVariables = !empty($variables) ? serialize($variables) : '';
      return hash('sha256', $query . $operationString . $stringVariables);
    }
    catch (\Exception $e) {
      // An exception might be thrown if the input variables are not serializable.
      // This is the case for mutations with file uploads where the variables
      // contain an instance of UploadedFile.
      return NULL;
    }
  }

  /**
   * Calculate the cache suffix for the current contexts.
   *
   * @param array $contexts
   *   The cache contexts.
   *
   * @return string
   *   The cache suffix.
   */
  protected function cacheSuffix(array $contexts = []): string {
    $keys = $this->contextsManager->convertTokensToKeys($contexts)->getKeys();
    return hash('sha256', serialize($keys));
  }

  /**
   * Lookup cached results by contexts for this query.
   *
   * @param string $prefix
   *   The cache prefix.
   *
   * @return \Drupal\Core\Cache\CachedResult|null
   *   The cached result.
   */
  protected function cacheRead(string $prefix): CachedResult|null {
    if (($cache = $this->cache->get("contexts:$prefix"))) {
      $suffix = $this->cacheSuffix($cache->data ?? []);
      if (($cache = $this->cache->get("result:$prefix:$suffix"))) {
        return $cache->data;
      }
    }

    return NULL;
  }

  /**
   * Store results in cache.
   *
   * @param string $prefix
   *   The cache prefix.
   * @param \Drupal\graphql\GraphQL\Execution\ExecutionResult $result
   *   The execution result.
   *
   * @return CachedResult|null
   *   The cached result.
   */
  protected function cacheWrite(string $prefix, ExecutionResult $result): CachedResult|null {
    $maxAge = $result->getCacheMaxAge();

    // Don't cache if max age is 0.
    if ($maxAge === 0) {
      return NULL;
    }

    $contexts = $result->getCacheContexts();
    $expire = $this->maxAgeToExpire($maxAge);
    $tags = $result->getCacheTags();
    $suffix = $this->cacheSuffix($contexts);

    $metadata = new CacheableMetadata();
    $metadata->addCacheableDependency($result);

    $cache = new CachedResult($result, $metadata, $expire);

    $this->cache->setMultiple([
      "contexts:$prefix" => [
        'data' => $contexts,
        'expire' => $expire,
        'tags' => $tags,
      ],
      "result:$prefix:$suffix" => [
        'data' => $cache,
        'expire' => $expire,
        'tags' => $tags,
      ],
    ]);

    return $cache;
  }

  /**
   * Maps a cache max age value to an "expire" value for the Cache API.
   *
   * @param int $maxAge
   *   The max age.
   *
   * @return int
   *   A corresponding "expire" value.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface::set()
   */
  protected function maxAgeToExpire(int $maxAge): int {
    $time = $this->requestStack->getMainRequest()->server->get('REQUEST_TIME');
    return ($maxAge === Cache::PERMANENT) ? Cache::PERMANENT : (int) $time + $maxAge;
  }

}
