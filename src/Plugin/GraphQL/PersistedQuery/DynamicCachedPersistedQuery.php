<?php

declare(strict_types=1);

namespace Drupal\graphql_extra_cache\Plugin\GraphQL\PersistedQuery;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\PersistedQuery\PersistedQueryPluginBase;
use GraphQL\Language\Parser;
use GraphQL\Server\OperationParams;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @PersistedQuery(
 *   id = "dynamic_cached_persisted_query",
 *   label = "Dynamic Cached Persisted Query",
 *   description = "Persists parsed queries dynamically."
 * )
 */
class DynamicCachedPersistedQuery extends PersistedQueryPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('cache.graphql_extra_cache.query'),
    );
  }

  /**
   * Constructs a new DynamicCachedPersistedQuery.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend for parsed queries.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    protected CacheBackendInterface $cache
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery($id, OperationParams $operation) {
    $cached = $this->cache->get($id);
    if ($cached && $cached->data) {
      return $cached->data;
    }

    // Get the query string and parse it and then store it in cache.
    $query = $operation->extensions['graphql_extra_cache']['query'] ?? NULL;
    if (!$query) {
      return NULL;
    }

    // Parse and store in cache.
    $parsed = Parser::parse($query, [
      'noLocation' => TRUE,
    ]);
    $this->cache->set($id, $parsed, CacheBackendInterface::CACHE_PERMANENT);
    return $parsed;
  }

}
