<?php

namespace Drupal\graphql_extra_cache\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\graphql_extra_cache\Controller\CachedRequestController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override the controller for the /graphql route.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    $storage = \Drupal::entityTypeManager()->getStorage('graphql_server');
    /** @var \Drupal\graphql\Entity\ServerInterface[] $servers */
    $servers = $storage->loadMultiple();

    foreach ($servers as $id => $server) {
      if ($server->schema === 'core_composable') {
        if ($route = $collection->get("graphql.query.{$id}")) {
          $route->setDefault(
            '_controller',
            CachedRequestController::class . '::handleRequest'
          );
        }
      }
    }
  }

}
